DROP TABLE IF EXISTS memory;
DROP TABLE IF EXISTS events;
DROP TABLE IF EXISTS conditions;
DROP TABLE IF EXISTS condition_types;
DROP TABLE IF EXISTS condition_frequencies;


DROP TABLE IF EXISTS response_conditions;
DROP TABLE IF EXISTS response_condition_types;
DROP TABLE IF EXISTS response_condition_groups;

DROP TABLE IF EXISTS condition_operators;

DROP TABLE IF EXISTS responses;
DROP TABLE IF EXISTS response_sets;
DROP TABLE IF EXISTS configs;
