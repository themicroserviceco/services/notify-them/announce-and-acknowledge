CREATE TABLE configs (
    ref BIGSERIAL PRIMARY KEY,
    organisation_ref BIGINT NULL,
    platform_ref BIGINT NULL,
    name TEXT NOT NULL,
    response_set_ref TEXT,
    expires_at TIMESTAMP WITH TIME ZONE DEFAULT 'infinity',
    created_at TIMESTAMP WITH TIME ZONE DEFAULT now(),
    created_by TEXT NOT NULL,
    created_by_token TEXT,
    modified_at TIMESTAMP WITH TIME ZONE DEFAULT now(),
    modified_by TEXT NOT NULL,
    modified_by_token TEXT
);

/**
  We build HASH indexes as these columns will ever only be equality operations, and HASH indexes significantly
  out perform B-Tree indexes over large data sets.
 */
CREATE INDEX i_configs_organisation_ref ON configs USING HASH (organisation_ref);
CREATE INDEX i_configs_platform_ref ON configs USING HASH (platform_ref);
CREATE INDEX i_configs_response_set_ref ON configs USING HASH (response_set_ref);

/**
  Tables for overall announcement conditions
 */
CREATE TABLE condition_operators (
    id              TEXT PRIMARY KEY,
    display         TEXT,
    condition_types TEXT[] DEFAULT NULL
);

INSERT INTO condition_operators (id, display) VALUES ('greater-than', 'Greater than');
INSERT INTO condition_operators (id, display) VALUES ('less-than', 'Less than');
INSERT INTO condition_operators (id, display) VALUES ('equal', 'Equal to');
INSERT INTO condition_operators (id, display) VALUES ('not-equal', 'Not equal to');
INSERT INTO condition_operators (id, display) VALUES ('in', 'In');
INSERT INTO condition_operators (id, display) VALUES ('not-in', 'Not in');
INSERT INTO condition_operators (id, display) VALUES ('between', 'Between');

CREATE TABLE condition_types (
    id TEXT PRIMARY KEY,
    display TEXT
);

INSERT INTO condition_types (id, display) VALUES ('input', 'Check input data');
INSERT INTO condition_types (id, display) VALUES ('timestamp-range', 'Timestamp range');
INSERT INTO condition_types (id, display) VALUES ('frequency', 'Frequency based');

CREATE TABLE condition_frequencies (
    id TEXT PRIMARY KEY,
    display TEXT
);

CREATE INDEX i_condition_operators_condition_types ON condition_operators USING GIN (condition_types);

CREATE TABLE conditions (
    ref BIGSERIAL PRIMARY KEY,
    config_ref BIGINT,
    organisation_ref BIGINT NULL,
    platform_ref BIGINT NULL,
    condition_order INTEGER,
    condition_type TEXT,
    -- timestamp-range
    timestamp_range TSTZRANGE DEFAULT NULL,
    -- input
    input_property TEXT DEFAULT NULL,
    input_value_operator TEXT DEFAULT NULL,
    input_value_text TEXT[] DEFAULT NULL,
    input_value_int INTEGER[] DEFAULT NULL,
    input_value_date DATE[] DEFAULT NULL,
    input_value_timestamp TIMESTAMP WITH TIME ZONE[] DEFAULT NULL,
    input_value_regex TEXT DEFAULT NULL,
    -- frequency
    frequency_identifier_input_property TEXT DEFAULT NULL,
    frequency_occurrence TEXT DEFAULT NULL,
    frequency_interval_unit TEXT DEFAULT NULL,
    frequency_interval_type TEXT DEFAULT NULL,
    frequency_interval_operator TEXT DEFAULT NULL,
    frequency_interval_value INTEGER DEFAULT NULL,
    frequency_event_name TEXT DEFAULT NULL,
    frequency_event_identifier TEXT DEFAULT NULL,
    frequency_output_years BOOLEAN DEFAULT FALSE,
    frequency_output_months BOOLEAN DEFAULT FALSE,
    frequency_output_weeks BOOLEAN DEFAULT FALSE,
    frequency_output_days BOOLEAN DEFAULT FALSE,
    frequency_output_hours BOOLEAN DEFAULT FALSE,
    frequency_output_minutes BOOLEAN DEFAULT FALSE,
    frequency_output_seconds BOOLEAN DEFAULT FALSE,

    -- Grouping configuration
    next_condition_grouped BOOLEAN DEFAULT false,
    next_condition_group_type TEXT DEFAULT 'and',
    next_condition_type TEXT DEFAULT 'and'
);

CREATE INDEX i_conditions_organisation_ref ON conditions USING HASH (organisation_ref);
CREATE INDEX i_conditions_platform_ref ON conditions USING HASH (platform_ref);

ALTER TABLE conditions
    ADD CONSTRAINT conditions_condition_types FOREIGN KEY (condition_type) REFERENCES condition_types (id);

ALTER TABLE conditions
    ADD CONSTRAINT conditions_configs FOREIGN KEY (config_ref) REFERENCES configs (ref);

ALTER TABLE conditions
    ADD CONSTRAINT conditions_condition_frequencies FOREIGN KEY (frequency_occurrence) REFERENCES condition_frequencies (id);

ALTER TABLE conditions
    ADD CONSTRAINT conditions_condition_frequency_operator FOREIGN KEY (frequency_interval_operator) REFERENCES condition_operators (id);

ALTER TABLE conditions
    ADD CONSTRAINT conditions_condition_input_operator FOREIGN KEY (input_value_operator) REFERENCES condition_operators (id);

CREATE UNIQUE INDEX ui_conditions_order_with_config ON conditions (config_ref, condition_order);
CREATE INDEX i_conditions_condition_order ON conditions (condition_order);

/**
    Responses
 */

CREATE TABLE response_sets (
    ref BIGSERIAL PRIMARY KEY,
    config_ref BIGINT,
    organisation_ref BIGINT NULL,
    platform_ref BIGINT NULL
);

CREATE INDEX i_response_sets_organisation_ref ON response_sets USING HASH (organisation_ref);
CREATE INDEX i_response_sets_platform_ref ON response_sets USING HASH (platform_ref);

CREATE TABLE response_condition_groups (
    ref BIGSERIAL PRIMARY KEY,
    config_ref BIGINT,
    organisation_ref BIGINT NULL,
    platform_ref BIGINT NULL,
    response_set_ref BIGINT,
    response_ref BIGINT
);

CREATE INDEX i_response_condition_groups_organisation_ref ON response_condition_groups USING HASH (organisation_ref);
CREATE INDEX i_response_condition_groups_platform_ref ON response_condition_groups USING HASH (platform_ref);

CREATE TABLE responses (
    ref BIGSERIAL PRIMARY KEY,
    config_ref BIGINT,
    organisation_ref BIGINT NULL,
    platform_ref BIGINT NULL,
    response_set_ref BIGINT,
    response_condition_group_ref BIGINT NULL,
    id TEXT,
    response_data JSONB
);

CREATE INDEX i_responses_organisation_ref ON responses USING HASH (organisation_ref);
CREATE INDEX i_responses_platform_ref ON responses USING HASH (platform_ref);

CREATE TABLE response_condition_types (
    id TEXT PRIMARY KEY,
    display TEXT
);

INSERT INTO response_condition_types (id, display) VALUES ('input', 'Check input data');
INSERT INTO response_condition_types (id, display) VALUES ('timestamp-range', 'Timestamp range');

CREATE TABLE response_conditions (
     ref BIGSERIAL PRIMARY KEY,
     config_ref BIGINT,
     organisation_ref BIGINT NULL,
     platform_ref BIGINT NULL,
     response_set_ref BIGINT,
     response_ref BIGINT,
     response_condition_group_ref BIGINT,
     condition_order INTEGER,
     condition_type TEXT,
     -- timestamp-range
     timestamp_range TSTZRANGE DEFAULT NULL,
     -- input
     input_property TEXT DEFAULT NULL,
     input_value_operator TEXT DEFAULT NULL,
     input_value_text TEXT[] DEFAULT NULL,
     input_value_int INTEGER[] DEFAULT NULL,
     input_value_date DATE[] DEFAULT NULL,
     input_value_timestamp TIMESTAMP WITH TIME ZONE[] DEFAULT NULL,
     input_value_regex TEXT DEFAULT NULL,
     -- Grouping configuration
     next_condition_grouped BOOLEAN DEFAULT false,
     next_condition_group_type TEXT DEFAULT 'and',
     next_condition_type TEXT DEFAULT 'and'
);

CREATE INDEX i_response_conditions_organisation_ref ON response_conditions USING HASH (organisation_ref);
CREATE INDEX i_response_conditions_platform_ref ON response_conditions USING HASH (platform_ref);

CREATE UNIQUE INDEX ui_response_conditions_order_with_group ON response_conditions (response_condition_group_ref, condition_order);
CREATE INDEX i_response_conditions_condition_order ON response_conditions (condition_order);

/**
  Because we've denormalised the tables for performance at scale. We need to add the constraints after the tables
  have been created as the order is completely independent.
 */

CREATE INDEX i_configs_response_sets ON configs USING HASH (response_set_ref);

ALTER TABLE response_sets
    ADD CONSTRAINT response_sets_configs FOREIGN KEY (config_ref) REFERENCES configs (ref);

ALTER TABLE responses
    ADD CONSTRAINT responses_configs FOREIGN KEY (config_ref) REFERENCES configs (ref);

ALTER TABLE responses
    ADD CONSTRAINT responses_response_sets FOREIGN KEY (response_set_ref) REFERENCES response_sets (ref);

CREATE INDEX i_responses_response_condition_groups ON responses USING HASH (response_condition_group_ref);

ALTER TABLE response_condition_groups
    ADD CONSTRAINT response_condition_groups_configs FOREIGN KEY (config_ref) REFERENCES configs (ref);

ALTER TABLE response_condition_groups
    ADD CONSTRAINT response_condition_groups_response_sets FOREIGN KEY (response_set_ref) REFERENCES response_sets (ref);

ALTER TABLE response_condition_groups
    ADD CONSTRAINT response_condition_groups_responses FOREIGN KEY (response_ref) REFERENCES responses (ref);

ALTER TABLE response_conditions
    ADD CONSTRAINT response_conditions_configs FOREIGN KEY (config_ref) REFERENCES configs (ref);

ALTER TABLE response_conditions
    ADD CONSTRAINT response_conditions_response_sets FOREIGN KEY (response_set_ref) REFERENCES response_sets (ref);

ALTER TABLE response_conditions
    ADD CONSTRAINT response_conditions_response_condition_groups FOREIGN KEY (response_condition_group_ref) REFERENCES response_condition_groups (ref);

ALTER TABLE response_conditions
    ADD CONSTRAINT response_conditions_responses FOREIGN KEY (response_ref) REFERENCES responses (ref);

ALTER TABLE response_conditions
    ADD CONSTRAINT response_conditions_response_condition_types FOREIGN KEY (condition_type) REFERENCES response_condition_types (id);

ALTER TABLE response_conditions
    ADD CONSTRAINT response_conditions_operator_types FOREIGN KEY (input_value_operator) REFERENCES condition_operators (id);

/**
  Tables for event management.
 */
CREATE TABLE events (
    ref BIGSERIAL PRIMARY KEY,
    organisation_ref BIGINT NULL,
    platform_ref BIGINT NULL,
    name TEXT NOT NULL,
    identifier TEXT,
    event_timestamp TIMESTAMP WITH TIME ZONE,
    custom_data JSONB DEFAULT NULL
);

CREATE INDEX i_events_organisation_ref ON events USING HASH (organisation_ref);
CREATE INDEX i_events_platform_ref ON events USING HASH (platform_ref);

CREATE INDEX i_events_identifier ON events (identifier);
CREATE INDEX i_events_event_timestamp ON events (event_timestamp);

CREATE TABLE memory (
    ref BIGSERIAL PRIMARY KEY,
    config_ref BIGINT,
    organisation_ref BIGINT NULL,
    platform_ref BIGINT NULL,
    response_ref BIGINT,
    identifier TEXT NOT NULL,
    memory_timestamp TIMESTAMP WITH TIME ZONE DEFAULT now(),
    keep_memory BOOLEAN DEFAULT false
);

CREATE INDEX i_memory_organisation_ref ON memory USING HASH (organisation_ref);
CREATE INDEX i_memory_platform_ref ON memory USING HASH (platform_ref);

ALTER TABLE memory
    ADD CONSTRAINT memory_configs FOREIGN KEY (config_ref) REFERENCES configs (ref);

ALTER TABLE memory
    ADD CONSTRAINT memory_responses FOREIGN KEY (response_ref) REFERENCES responses (ref);

CREATE INDEX i_memory_identifier ON memory (identifier);
CREATE INDEX i_memory_memory_timestamp ON memory (memory_timestamp);
CREATE INDEX i_memory_keep_memory ON memory (keep_memory);
