CREATE OR REPLACE FUNCTION sp_create_config ( p_name TEXT,
	p_response_set JSON,
	p_organisation_uuid TEXT DEFAULT NULL,
	p_platform_uuid TEXT DEFAULT NULL,
	p_expires_at TIMESTAMP WITH TIME ZONE DEFAULT 'infinity')
RETURNS TABLE (
	status TEXT,
	message TEXT,
	ref BIGINT)

AS $$

    DECLARE l_config_ref BIGINT;

    DECLARE l_responses JSON;

    DECLARE l_conditions JSON;
    DECLARE l_condition_ref BIGINT;
    DECLARE l_condition_order INT;
    DECLARE l_condition_query TEXT;
    DECLARE l_condition RECORD;
BEGIN

	l_config_ref := NEXTVAL('configs_ref_seq');
	l_condition_order := 1;

	-- First, let's create the configuration record.
	EXECUTE FORMAT('INSERT INTO configs(ref, name, expires_at) VALUES (%L,%L,%L);',
		l_config_ref,
		p_name,
		p_expires_at
	);

	-- Now let's grab all the parts of the configuration record for processing
	l_conditions := json_extract_path(p_response_set, 'conditions');
	l_responses := json_extract_path(p_response_set, 'responses');

	-- Build the insert query for the conditions.
	l_condition_query := <<SQL
	    INSERT INTO conditions (
            ref,
            config_ref,
            organisation_ref,
            platform_ref,
            condition_order,
            condition_type,
            timestamp_range,
            input_property,
            input_value_operator,
            input_value_text,
            input_value_int,
            input_value_date,
            input_value_timestamp,
            input_value_regex,
            frequency_identifier_input_property,
            frequency_occurrence,
            frequency_interval_unit,
            frequency_interval_type,
            frequency_interval_operator,
            frequency_interval_value,
            frequency_event_name,
            frequency_event_identifier,
            frequency_output_years,
            frequency_output_months,
            frequency_output_weeks,
            frequency_output_days,
            frequency_output_hours,
            frequency_output_minutes,
            frequency_output_seconds,
            next_condition_grouped,
            next_condition_group_type,
            next_condition_type
	    ) VALUES (
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L,
             %L
        );
SQL;

	-- Loop through the supplied conditions and create them.
	FOR l_condition IN SELECT json_populate_recordset(l_conditions) as t(
	    condition_type text,
        timestamp_range text[],
        input_property text,
        input_value_operator text,
        input_value_text text[],
        input_value_int int[],
        input_value_date date[],
        input_value_timestamp timestamp with time zone[],
        input_value_regex text,
        frequency_identifier_input_property text,
        frequency_occurrence text,
        frequency_interval_unit text,
        frequency_interval_type text,
        frequency_interval_operator text,
        frequency_interval_value int,
        frequency_event_name text,
        frequency_event_identifier text,
        frequency_output_years boolean,
        frequency_output_months boolean,
        frequency_output_weeks boolean,
        frequency_output_days boolean,
        frequency_output_hours boolean,
        frequency_output_minutes boolean,
        frequency_output_seconds boolean,
        next_condition_grouped boolean,
        next_condition_group_type text,
        next_condition_type text)
    LOOP

	    l_condition_ref = NEXTVAL('conditions_ref_seq')

        EXECUTE FORMAT(l_condition_query,
            l_condition_ref,
            l_config_ref,
            p_organisation_uuid,
            p_platform_uuid,
            l_condition_order,
            l_condition.condition_type,
            l_condition.timestamp_range,
            l_condition.input_property,
            l_condition.input_value_operator,
            l_condition.input_value_text,
            l_condition.input_value_int,
            l_condition.input_value_date,
            l_condition.input_value_timestamp,
            l_condition.input_value_regex,
            l_condition.frequency_identifier_input_property,
            l_condition.frequency_occurrence,
            l_condition.frequency_interval_unit,
            l_condition.frequency_interval_type,
            l_condition.frequency_interval_operator,
            l_condition.frequency_interval_value,
            l_condition.frequency_event_name,
            l_condition.frequency_event_identifier,
            l_condition.frequency_output_years,
            l_condition.frequency_output_months,
            l_condition.frequency_output_weeks,
            l_condition.frequency_output_days,
            l_condition.frequency_output_hours,
            l_condition.frequency_output_minutes,
            l_condition.frequency_output_seconds,
            l_condition.next_condition_grouped,
            l_condition.next_condition_group_type,
            l_condition.next_condition_type
        );

	    l_condition_order = l_condition_order + 1;

    end loop;

    ref = l_config_ref;
	status = 'ok';
	message = 'All good.';

	RETURN NEXT;
    RETURN;
END;

$$ LANGUAGE PLpgSQL;