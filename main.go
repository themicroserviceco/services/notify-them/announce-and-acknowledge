package main

import (
	"fmt"
	"log"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"

	"gitlab.com/themicroserviceco/services/notify-them/announce-and-acknowledge/internal/handlers"
)

func index(ctx *fasthttp.RequestCtx) {
	_, _ = fmt.Fprint(ctx, "Welcome!\n")
}

func fetchAnnouncementByName(ctx *fasthttp.RequestCtx) {
	_, _ = fmt.Fprintf(ctx, "fetching announcements of type: %s!\n", ctx.UserValue("name"))

}

func fetchAnnouncementByUUID(ctx *fasthttp.RequestCtx) {
	_, _ = fmt.Fprintf(ctx, "fetching announcements of type uuid: %s!\n", ctx.UserValue("uuid"))

}

func main() {
	router := fasthttprouter.New()

	router.GET("/", index)
	router.POST("/create", handlers.CreateAnnouncementHandler)
	//router.POST("/fetch-all", fetchAllAnnouncements)
	router.GET("/fetch-by-name/:name", fetchAnnouncementByName)
	router.GET("/fetch-by-uuid/:uuid", fetchAnnouncementByUUID)

	log.Fatal(fasthttp.ListenAndServe(":8080", router.Handler))
}
