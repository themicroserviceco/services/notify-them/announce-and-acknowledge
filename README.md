# Announce & Acknowledge (A&A)

Targeted in-app announcements and messaging is simple - except when it isn't. A&A is part of the Notify Them suite
of services designed to allow customised, targeted and "rememberable" announcements for your apps.

## How it works

A&A is a polled service (we don't push notifications - that's our "BossyBoots" service), that allows you to check, with
lightening speed whether a user/device/whatever needs to be shown a message. 

These messages are defined and created on our service with conditions - based on the data you send us, we determine
whether a condition has been met, and return the relevant announcements for that user/device/whatever.

In more complex use cases, your app can send events to A&A to use in determining announcements. For example, you want
to enforce a password rotation policy - your backend can send us an event every time a user changes their password. You
then setup announcements for "expiring soon", and "expired". These can be configured to look at the events your app has been
sending to determine whether the particular use needs to change their password.


## Working locally

This service can be used stand-alone and doesn't actually need to connect via the IAM services. You simply set the env
variable "NO_IAM" to True, and it will prevent running the requests through the Authentication middleware.

### The power of Make

In order to make local dev simple. We use make files to group logical commands together. The makefile has no idea
whether you are using docker-machine (which you should) or just straight docker on your 'puter. Therefore, if you use
docker-machine ensure ensure you are in the correct environment before running any make commands.

You can load the environment of a docker-machine using: 

```eval $(docker-machine env <machine-name>)```

The following commands are available, they are largely self explanatory.

#### build

Builds all of the containers that reference Dockerfiles - it does not build run-time containers (like postgres)

```make build```

#### start
Starts the docker containers in detached mode.

```make start```

#### restart
Restarts the docker containers using "force-recreate". Very useful when you change env variables.

```make restart```

#### logs c=<container-name>
Creates a log stream in the terminal for the provided container. For this package, you might want to see the output of
the application service:

```make logs c=app```

#### psql
A short cut into the psql terminal for the postgres container. You will need to enter the password manually, and relies
on the default database names/usernames in the .envrc file committed to the repo. If you change these, you will need
to change the make file.

```make psql```


#### clean
Runs an image prune on the docker environment. The downside of clean, is it will remove the containers associated with
building your production alpine app containers - which means a rebuild will need to redownload the mod packages for go. 

The upside, it will remove the containers associated with building your production alpine app containers - freeing up
space.

```make clean```

#### stop
Brings down all containers defined in the docker-compose.yaml.

#### wipeout
*** USE THIS WITH CARE - IT WILL WIPE OUT YOUR DOCKER CONTAINERS - NOT JUST THE ONES ASSOCIATED WITH THE DOCKER-COMPOSE ***

This is brutal as shit. Use only when you're experiencing an issue and just can't figure it out - and are more than happy
to lose everything data wise (including database volumes etc).

```make wipeout```

#### redo

This runs the following other make commands in order (It's a shortcut command):
stop -> wipeout -> build -> start

```make redo```

#### rebuild

This runs the following other make commands in order (It's a shortcut command):
build -> restart

```make rebuild```

#### rebuild-clean

This runs the following other make commands in order (It's a shortcut command):
build -> restart -> clean

```make rebuild-clean```

#### app-downgrade:

This runs the downgrade database migration command for the app. It's used to reverse the last migration that was released.

```make app-downgrade```

#### app-upgrade:
This runs the upgrade database migration command for the app. It's used to release the next version of your schema.

```make app-upgrade```

#### app-refresh-procedures:
This runs the refresh procedures migration command for the app. Essentially loops through all the up files in the 
datastore/procedures folder, and releases them. In order for this to keep track properly, you should always have corresponding
empty .down.sql files for each up. We run a downgrade first so that we don't have to add new sequences to our stored procedures
when we want to make a change to them. Source control can handle diffing these.

```make app-refresh-procedures```


