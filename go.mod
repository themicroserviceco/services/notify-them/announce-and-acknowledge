module gitlab.com/themicroserviceco/services/notify-them/announce-and-acknowledge

go 1.14

require (
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/golang-migrate/migrate/v4 v4.11.0 // indirect
	github.com/lib/pq v1.7.0 // indirect
	github.com/valyala/fasthttp v1.14.0
)
