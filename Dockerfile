FROM golang:1.14.3-stretch AS builder

ENV GO111MODULE=on

WORKDIR /build

# Let's cache modules retrieval - those don't change so often
COPY go.mod .
COPY go.sum .
RUN go mod download

# Copy the code necessary to build the application
# You may want to change this to copy only what you actually need.
COPY . .

# Build the application
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ./run ./main.go
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ./migrate ./cmd/migration/main.go

# Let's create a /dist folder containing just the files necessary for runtime.
# Later, it will be copied as the / (root) of the output image.
WORKDIR /dist
RUN cp /build/run ./run
RUN cp /build/migrate ./migrate

# Optional: in case your application uses dynamic linking (often the case with CGO),
# this will collect dependent libraries so they're later copied to the final image
# NOTE: make sure you honor the license terms of the libraries you copy and distribute
RUN ldd run | tr -s '[:blank:]' '\n' | grep '^/' | \
    xargs -I % sh -c 'mkdir -p $(dirname ./%); cp % ./%;'
RUN ldd migrate | tr -s '[:blank:]' '\n' | grep '^/' | \
    xargs -I % sh -c 'mkdir -p $(dirname ./%); cp % ./%;'
RUN mkdir -p lib64 && cp /lib64/ld-linux-x86-64.so.2 lib64/

# Copy or create other directories/files your app needs during runtime.
RUN mkdir -p /dist/cmd
RUN cp /build/cmd/*.sh ./cmd/

RUN mkdir -p /dist/datastore/migrations
RUN cp /build/datastore/migrations/*.sql ./datastore/migrations/

RUN mkdir -p /dist/datastore/procedures
RUN cp /build/datastore/procedures/*.sql ./datastore/procedures/

# Create the minimal runtime image
FROM alpine:3.12.0
COPY --from=builder /dist /app
RUN chmod +x /app/cmd/entrypoint.sh
RUN chmod +x /app/cmd/startapp.sh
RUN chmod +x /app/cmd/migrate.sh

ENTRYPOINT ["/app/cmd/entrypoint.sh"]
CMD ["/app/cmd/startapp.sh"]