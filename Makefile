build:
	docker-compose build

start:
	docker-compose up -d

restart:
	docker-compose up -d --force-recreate

logs:
	docker-compose logs -f $(c)

psql:
	docker-compose run --rm postgres psql -h postgres -U dbuser app

clean:
	docker image prune

stop:
	docker-compose down

wipeout:
	docker system prune -a --force

redo: stop wipeout build start

rebuild: build restart

rebuild-clean: build restart clean

app-downgrade:
	docker-compose run --rm app /app/migrate --action=downgrade

app-upgrade:
	docker-compose run --rm app /app/migrate --action=upgrade

app-refresh-procedures:
	docker-compose run --rm app /app/migrate --action=refresh-procedures
