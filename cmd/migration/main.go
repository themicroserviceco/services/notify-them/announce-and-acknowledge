package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

func main() {

	var action string
	flag.StringVar(&action, "action", "", "Usage")
	flag.Parse()

	fmt.Println("Selected action: " + action + "!")

	m, err := migrate.New(
		os.Getenv("DB_MIGRATIONS_PATH"),
		os.Getenv("DB_URI"))

	if err != nil {
		fmt.Println("Error starting migrations", err)
		return
	}

	if action == "upgrade" {

		if err := m.Up(); err != nil {

			fmt.Println("Error", err)

		} else {

			fmt.Println(action, "run successfully")
		}

	} else if action == "downgrade" {

		if err := m.Down(); err != nil {

			fmt.Println("Error", err)

		} else {

			fmt.Println(action, "run successfully")
		}

	} else if action == "refresh-procedures" {
		sm, err := migrate.New(
			os.Getenv("DB_PROCEDURES_PATH"),
			os.Getenv("DB_URI"))

		if err != nil {
			fmt.Println("Error starting procedures refresh", err)
			return
		}

		if err := sm.Down(); err != nil {
			fmt.Println("error", err)
		} else {
			if err := sm.Up(); err != nil {
				fmt.Println("Error", err)

			} else {
				fmt.Println(action, "Procedures successfully refreshed")
			}
		}

	} else {
		fmt.Println("action", action, "not valid")
	}
}
