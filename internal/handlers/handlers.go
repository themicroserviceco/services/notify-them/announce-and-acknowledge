package handlers


import (
	"encoding/json"
	//"encoding/json"
	"fmt"
	"gitlab.com/themicroserviceco/services/notify-them/announce-and-acknowledge/internal/util"

	"github.com/valyala/fasthttp"
)

// CreateAnnouncementHandler ...
func CreateAnnouncementHandler(ctx *fasthttp.RequestCtx) {

	ctx.SetContentType("application/json; charset=UTF-8")

	request, errD := util.ParseCreateAnnouncementRequest(ctx.PostBody())

	if errD != nil {

		ctx.SetBody([]byte("{}"))
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}

	bt, err := json.Marshal(&request)

	if err != nil {

		ctx.SetBody([]byte("{}"))
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)

	}

	ctx.SetBody(bt)
	ctx.SetStatusCode(fasthttp.StatusOK)

	fmt.Println("LOG CreateAnnouncementHandler: Created")
	return
}
