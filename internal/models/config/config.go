package config

type Config struct {
	UUID            string `json:"uuid,omitempty"`
	OrganisationRef string `json:"organisation_ref"`
	PlatformRef     string `json:"platform_ref"`
	ResponseSetRef  string `json:"response_set_ref,omitempty"`
	Name            string `json:"name"`
	ExpiresAt       string `json:"expires_at"`
	CreatedAt       string `json:"created_at"`
	CreatedBy       string `json:"created_by"`
	CreatedByToken  string `json:"created_by_token"`
	ModifiedAt      string `json:"modified_at"`
	ModifiedBy      string `json:"modified_by"`
	ModifiedByToken string `json:"modified_by_token"`
}
