package event

import "encoding/json"

// Event ...
type Event struct {
	Ref             string          `json:"ref,omitempty"`
	OrganisationRef string          `json:"organisation_ref"`
	PlatformRef     string          `json:"platform_ref"`
	Name            string          `json:"name"`
	Identifier      string          `json:"identifier"`
	EventTimestamp  string          `json:"event_timestamp"`
	CustomData      json.RawMessage `json:"custom_data"`
}
