package memory

// Memory ...
type Memory struct {
	Ref             string `json:"ref,omitempty"`
	ConfigRef       string `json:"config_ref"`
	OrganisationRef string `json:"organisation_ref"`
	PlatformRef     string `json:"platform_ref"`
	ResponseRef     string `json:"response_ref"`
	Identifier      string `json:"identifier"`
	MemoryTimestamp string `json:"memory_timestamp"`
	KeepMemory      bool   `json:"keep_memory"`
}
