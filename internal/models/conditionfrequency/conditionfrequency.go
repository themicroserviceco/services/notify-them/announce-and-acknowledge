package conditionfrequency

// ConditionFrequency ...
type ConditionFrequency struct {
	Id      string `json:"id"`
	Display string `json:"display"`
}
