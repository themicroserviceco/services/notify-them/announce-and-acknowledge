package responseset

// ResponseSet ...
type ResponseSet struct {
	Ref             string `json:"ref,omitempty"`
	ConfigRef       string `json:"config_ref"`
	OrganisationRef string `json:"organisation_ref"`
	PlatformRef     string `json:"platform_ref"`
}