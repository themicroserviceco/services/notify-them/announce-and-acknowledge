package conditiontype

// ConditionType ...
type ConditionType struct {
	Id      string `json:"id"`
	Display string `json:"display"`
}