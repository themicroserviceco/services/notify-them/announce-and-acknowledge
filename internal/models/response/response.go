package response

import "encoding/json"

// Response ...
type Response struct {
	Ref                       string          `json:"ref,omitempty"`
	ConfigRef                 string          `json:"config_ref"`
	OrganisationRef           string          `json:"organisation_ref"`
	PlatformRef               string          `json:"platform_ref"`
	ResponseSetRef            string          `json:"response_set_ref"`
	ResponseConditionGroupRef string          `json:"response_condition_group_ref"`
	Id                        string          `json:"id"`
	ResponseData              json.RawMessage `json:"response_data"`
}
