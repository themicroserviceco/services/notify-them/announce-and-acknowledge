package responseconditiongroup

// ResponseConditionGroup ...
type ResponseConditionGroup struct {
	Ref             string `json:"ref,omitempty"`
	ConfigRef       string `json:"config_ref"`
	OrganisationRef string `json:"organisation_ref"`
	PlatformRef     string `json:"platform_ref"`
	ResponseSetRef  string `json:"response_set_ref"`
	ResponseRef     string `json:"response_ref"`
}