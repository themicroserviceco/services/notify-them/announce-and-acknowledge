package responsecondition

// ResponseCondition ...
type ResponseCondition struct {
	Ref                       string      `json:"ref,omitempty"`
	ConfigRef                 string      `json:"config_ref"`
	OrganisationRef           string      `json:"organisation_ref"`
	PlatformRef               string      `json:"platform_ref"`
	ResponseSetRef            string      `json:"response_set_ref"`
	ResponseRef               string      `json:"response_ref"`
	ResponseConditionGroupRef string      `json:"response_condition_group_ref"`
	ConditionOrder            int         `json:"condition_order"`
	ConditionType             Type        `json:"condition_type"`
	TimestampRange            []string    `json:"timestamp_range"`
	InputProperty             string      `json:"input_property"`
	InputValueOperator        Operator    `json:"input_value_operator"`
	InputValueText            []string    `json:"input_value_text"`
	InputValueInt             []string    `json:"input_value_int"`
	InputValueDate            []string    `json:"input_value_date"`
	InputValueTimestamp       []string    `json:"input_value_timestamp"`
	InputValueRegex           string      `json:"input_value_regex"`
	NextConditionGrouped      bool        `json:"next_condition_grouped"`
	NextConditionGroupType    Conditional `json:"next_condition_group_type"`
	NextConditionType         Conditional `json:"next_condition_type"`
}
