package responsecondition

// Type enum
type Type string

const (
	// Input ...
	Input Type = "input"
	// Timestamp Range ...
	TimestampRange Type = "timestamp-range"
)

// Conditional enum
type Conditional string

const (
	// And ...
	And Conditional = "and"
	// Or ...
	Or Conditional = "or"
)

// Operator enum
type Operator string

const (
	GreaterThan Operator = "greater-than"
	LessThan    Operator = "less-than"
	EqualTo     Operator = "equal"
	NotEqualTo  Operator = "not-equal"
	In          Operator = "in"
	NotIn       Operator = "not-in"
	Between     Operator = "between"
)
