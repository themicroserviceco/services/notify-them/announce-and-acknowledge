package condition

// Condition ...
type Condition struct {
	Ref                              string       `json:"ref,omitempty"`
	ConfigRef                        string       `json:"config_ref"`
	OrganisationRef                  string       `json:"organisation_ref"`
	PlatformRef                      string       `json:"platform_ref"`
	ConditionOrder                   int          `json:"condition_order"`
	ConditionType                    Type         `json:"condition_type"`
	TimestampRange                   []string     `json:"timestamp_range"`
	InputProperty                    string       `json:"input_property"`
	InputValueOperator               Operator     `json:"input_value_operator"`
	InputValueText                   []string     `json:"input_value_text"`
	InputValueInt                    []string     `json:"input_value_int"`
	InputValueDate                   []string     `json:"input_value_date"`
	InputValueTimestamp              []string     `json:"input_value_timestamp"`
	InputValueRegex                  string       `json:"input_value_regex"`
	FrequencyIdentifierInputProperty string       `json:"frequency_identifier_input_property"`
	FrequencyOccurrence              Occurrence   `json:"frequency_occurrence"`
	FrequencyIntervalUnit            IntervalUnit `json:"frequency_interval_unit"`
	FrequencyIntervalType            IntervalType `json:"frequency_interval_type"`
	FrequencyIntervalOperator        Operator     `json:"frequency_interval_operator"`
	FrequencyIntervalValue           []int        `json:"frequency_interval_value"`
	FrequencyEventName               string       `json:"frequency_event_name"`
	FrequencyEventIdentifier         string       `json:"frequency_event_identifier"`
	FrequencyOutputYears             bool         `json:"frequency_output_years"`
	FrequencyOutputMonths            bool         `json:"frequency_output_months"`
	FrequencyOutputWeeks             bool         `json:"frequency_output_weeks"`
	FrequencyOutputDays              bool         `json:"frequency_output_days"`
	FrequencyOutputHours             bool         `json:"frequency_output_hours"`
	FrequencyOutputMinutes           bool         `json:"frequency_output_minutes"`
	FrequencyOutputSeconds           bool         `json:"frequency_output_seconds"`
	NextConditionGrouped             bool         `json:"next_condition_grouped"`
	NextConditionGroupType           Conditional  `json:"next_condition_group_type"`
	NextConditionType                Conditional  `json:"next_condition_type"`
}
