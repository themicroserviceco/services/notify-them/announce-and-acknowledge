package condition

// Type enum
type Type string

const (
	// Frequency ...
	Frequency Type = "frequency"
	// Input ...
	Input Type = "input"
	// Timestamp Range ...
	TimestampRange Type = "timestamp-range"
)

// Conditional enum
type Conditional string

const (
	// And ...
	And Conditional = "and"
	// Or ...
	Or Conditional = "or"
)

// Occurrence enum
type Occurrence string

const (
	Always Occurrence = "always"
	Event  Occurrence = "event"
	Once   Occurrence = "once"
)

// IntervalUnit enum
type IntervalUnit string

const (
	Seconds IntervalUnit = "seconds"
	Minutes IntervalUnit = "minutes"
	Hours   IntervalUnit = "hours"
	Days    IntervalUnit = "days"
	Weeks   IntervalUnit = "weeks"
	Months  IntervalUnit = "months"
	Years   IntervalUnit = "years"
)

// IntervalUnit enum
type IntervalType string

const (
	Since IntervalType = "since"
	Until IntervalType = "until"
)

// Operator enum
type Operator string

const (
	GreaterThan Operator = "greater-than"
	LessThan    Operator = "less-than"
	EqualTo     Operator = "equal"
	NotEqualTo  Operator = "not-equal"
	In          Operator = "in"
	NotIn       Operator = "not-in"
	Between     Operator = "between"
)
