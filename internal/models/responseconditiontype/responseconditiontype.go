package responseconditiontype

// ResponseConditionType ...
type ResponseConditionType struct {
	Id      string `json:"id"`
	Display string `json:"display"`
}

