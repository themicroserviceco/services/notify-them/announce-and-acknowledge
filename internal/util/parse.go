package util

import (
	"encoding/json"
	"fmt"

	"gitlab.com/themicroserviceco/services/notify-them/announce-and-acknowledge/internal/models/config"
)

// ParseCreateAnnouncementRequest ...
func ParseCreateAnnouncementRequest(payload []byte) (*config.Config, error) {

	request := &config.Config{}

	fmt.Println("LOG Config/router@Config: Parsing Announcement Configuration.....")

	if errD := json.Unmarshal(payload, request); errD != nil {

		fmt.Println("ERR Config/router@Config: Error decoding request", errD)
		return nil, errD
	}

	return request, nil
}
